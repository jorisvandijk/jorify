# This project is now Archived
This project was a great learning experience for me. It made me understand the installation process of [Arch Linux](https://archlinux.org) so much better and in more detail than I did before. However, I am dropping the use of it in favor of [Anarchy Installer](https://anarchyinstaller.gitlab.io/). The one downside is that I am going back to having to go through menus and select my settings manually, instead of automating the whole thing. Both methods are about the same in terms of installation time, so rather than maintain a script I'll rely on an already maintained script.

# Jorify
This is a script that will set up a fresh Arch install to my exact liking.

It **will not** work as-is for you. If you have a read through the thing, you'll see what I mean.
Feel free to play around with it and make it yours, if you're so inclined. 

## How to use
First, **read through the code**! There are several things that need to be altered if you want to use this for yourself.

Second launch into a fresh Arch Linux install. Then grab this script.
```
curl -L tiny.cc/jorify > jorify
```

Now launch it.
```
sh jorify
```

Babysit it for a few seconds to enter some credentials and then let it do its thing.

## Warrenty
None. Obviously. Using this will delete everything from your system and end the world... or, something like it. At the very least expect your system not to work, when ran without editing. I take no responsibility for any mishaps.
