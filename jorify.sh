#!/bin/bash
#
# Usage: Jorify my fresh Arch install
#
# !!! DO NOT RUN THIS SCRIPT, IT'LL DESTROY YOUR SYSTEM!!!
# 
# Script by Joris van Dijk | gitlab.com/jorisvandijk 
#
#          Published under GPL-3.0-or-later

# Work from HOME
cd $HOME

# Refreshing!
sudo pacman -Syyu --noconfirm

# Set up Git
git config --global user.email joris@jorisvandijk.com
git config --global user.name joris
git clone https://gitlab.com/jorisvandijk/kee.git
mv kee/ssh/.ssh .ssh
chmod 700 .ssh
chmod 644 .ssh/id_rsa.pub
chmod 600 .ssh/id_rsa
rm -rf kee

# Install yay
git clone https://aur.archlinux.org/yay.git
cd yay
makepkg -si --noconfirm
cd $HOME
rm -rf yay
sudo pacman -R --noconfirm go

# Swap LightDM greeter
#yay -S --noconfirm lightdm-mini-greeter
#sudo pacman -Rs --noconfirm lightdm-gtk-greeter

# Move LightDM configurations
#curl -L https://gitlab.com/jorisvandijk/jorify/-/raw/master/lightdm.conf > lightdm.conf
#sudo mv lightdm.conf /etc/lightdm/lightdm.conf
#curl -L https://gitlab.com/jorisvandijk/jorify/-/raw/master/lightdm-mini-greeter.conf > lightdm-mini-greeter.conf
#sudo mv lightdm-mini-greeter.conf /etc/lightdm/lightdm-mini-greeter.conf

# Install pacman packages
#curl -L https://gitlab.com/jorisvandijk/jorify/-/raw/master/pkglist > pkglist
#sudo pacman -S --needed --noconfirm $(cat pkglist|xargs)
#rm pkglist

# Install AUR packages
#curl -L https://gitlab.com/jorisvandijk/jorify/-/raw/master/pkglist_aur > pkglist_aur
#yay -S --noconfirm --needed --removemake  $(cat pkglist_aur|xargs)
#rm pkglist_aur

# Repositories
mkdir Git
cd Git

git clone git@gitlab.com:jorisvandijk/dotfiles.git

#curl -L https://gitlab.com/jorisvandijk/jorify/-/raw/master/repos > repos
#xargs -n1 git clone < repos 
cd $HOME

# Dotfiles
rm -rf .config
rm .bashrc 
cd Git/dotfiles
for d in *; do stow -v -t ~ "$d" ;done
cd $HOME

# Kee
#cd Git/kee/stow
#for d in *; do stow -v -t ~ "$d" ;done
#cd $HOME

# Fix for Virtualbox
#sudo modprobe vboxdrv

# Bye!
echo "We're done!"
exit
